#!/bin/bash

# loop a command a couple of times
dce_loop() {
    for _ in $(seq 1 5); do
        if "$@"; then
            local s=0
            break
        else
            local s=$?
            sleep 10
        fi
    done
    return $s
}

# run a command either from the current directory or via PATH
dce_run_locally() {
    local prog=$1
    shift 1
    if [ -f "./$prog" ]; then
        "./$prog" "$@"
    else
        "$prog" "$@"
    fi
}

# determine container image script variables
#   IMAGE_NAME: from CI_JOB_NAME without */ prefix if IMAGE_NAME is not defined
#   IMAGE_TAGS: from CI_COMMIT_SHORT_SHA/CI_MERGE_REQUEST_IID/CI_COMMIT_REF_NAME
dce_determine_image_variables() {
    if ! [ -v IMAGE_NAME ] && [ -v CI_JOB_NAME ]; then
        declare -g IMAGE_NAME=${CI_JOB_NAME#*/}
    fi
    declare -ga IMAGE_TAGS=()
    if [ -v DEPLOY_TAG ]; then
        IMAGE_TAGS+=("$DEPLOY_TAG")
    else
        [ -v CI_COMMIT_SHORT_SHA ] && IMAGE_TAGS+=("git-$CI_COMMIT_SHORT_SHA")
        [ -v CI_MERGE_REQUEST_IID ] && IMAGE_TAGS+=("mr-$CI_MERGE_REQUEST_IID")
        [ -v CI_COMMIT_REF_NAME ] && [ "$CI_COMMIT_REF_NAME" = "$CI_DEFAULT_BRANCH" ] && IMAGE_TAGS+=("latest")
    fi
    return 0
}
